# GitLab Screenshare mode

This is a tiny extension that allows to hide confidential information on your GitLab screen.
This might be useful for screen shares, in order not to leak priviliged information.
Currently supports hiding:

- Confidential issues:
   - Issues detail page. Checks if the issue is confidential:
      - If not: Shows the issue
      - If yes: Hides issue description and adds a button instead
   - Issue boards: Replaces confidential issues with "empty" cards. (Click still opens the side-drawer though)
   - Issue lists: Replaces confidential issues with a redacted list
- Confidential paths (currently all within `/gitlab-com/account-management`):
    - Redacts system notes pointing to projects
    - Redacts linked issues pointing to projects
    - Redacts projects from main project list


## How to install

First, clone this repo `git clone git@gitlab.com:leipert-projects/gitlab-screenshare-mode.git` and then see below for browser specific instructions.

### Chrome

Recently chrome disallowed to install packed `crx` extension that are not listed on the Chrome Store, so to install this

- On Chrome: Menu
  - More Tools
    - Extensions
- In the Extension page: `Load unpacked` and select the cloned repository

### Firefox

- On Firefox: enter `about:debugging#/runtime/this-firefox` into the address bar
- In the Extension page: `Load Temporary Add-on...` and select any file within the cloned repository

## How to update

- `git pull`

### Chrome

- On Chrome: Menu
  - More Tools
    - Extensions
- In the Extension page find `GitLab Screenshare mode` and hit the refresh button

### Firefox

- On Firefox: enter `about:debugging#/runtime/this-firefox` into the address bar
- In the Extension page find `GitLab Screenshare mode` and hit the reload button
