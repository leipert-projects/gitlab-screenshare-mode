iconsrc := icons/icon-512.png
icondir := icons/
iconsizes := {16,19,38,48,128,256}
iconfiles := $(shell echo $(icondir)/icon-$(iconsizes).png)

$(icondir)/icon-%.png:
	@mkdir -p $(@D)
	convert $(iconsrc) -resize $* $@

icons: $(iconfiles)

src/inject.js: inject.css
	node ./injectCSS.js

gitlab-screenshare-mode.zip: icons src/inject.js
	rm -f $@
	zip -r $@ _locales/ icons/ src/ manifest.json

.PHONY: icons src/inject.js

.DEFAULT_GOAL := gitlab-screenshare-mode.zip
